<?php
?>
			<div class="post <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
				<p class="header">
					<?php if ($comment->new) { ?> 
						<span class="new"><?php print $new ?></span>
					<?php }; ?>
					<div class="comment">
						<?php if ($picture) { ?>
							<?php print $picture ?>
						<?php }; ?>
						<span class="author-name"><?php print $author ?></span> | <span class="comment-date"><?php print $date ?></span>
						<div class="content"><?php print $content ?></div> 
						<div class="footer">
							<span class="commentlinks"><?php print $links ?></li></span>
						</div> 
					</div>
				</p>
			</div>