<?php
if (isset($node->links)) {
    foreach($node->links as $item){
      $split = preg_split("/(<a.*>|<\/a>)/U", $item, 0, PREG_SPLIT_NO_EMPTY);
      $my_links[$split[0]] = $item;
    }
}
?>

<div class="post<?php print ($sticky) ? " sticky" : ""; ?>">
	<?php if ($page == 0): ?>
	<div class="header">
		<h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
			<?php if($submitted): ?>
				<span class="submitted"><?php print $submitted; ?></span>
			<?php endif; ?>
	</div>
	<?php endif; ?>
	<?php print $picture ?>

	<div class="content">
		<?php print $content ?>
	</div>
	<div class="footer">
			<?php if ($terms): ?>
				<span class="tagdata">
				<?php print $terms ?>
				</span>
			<?php endif; ?>
			<?php if ($links): ?>
				<span class="commentlinks">
				<?php print $links ?>
				</span>
			<?php endif; ?>
	</div>
</div>
