<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<?php print $head; ?>
<?php print $styles; ?>
<title><?php print $head_title; ?></title>
</head>

<body>

<div id="outer">

	<div id="upbg"></div>

	<div id="inner">

		<div id="header">
			<?php if ($site_name) : ?>
			<h1><?php print($site_name); ?></h1>
			<?php endif; ?>
			<?php if ($site_slogan) : ?>
			<h2><?php print($site_slogan) ?></h2>
			<?php endif; ?>
			<?php if (function_exists("translation_url")): ?>
				<div class="language">
				<?php 
					if (function_exists('translation_get_links')) { 
						$trans_links = translation_get_links('', 0); 
						foreach ($trans_links as $value) {
							echo $value;
						}
					} 
				?>
				</div>
				<?php endif; ?>
		</div>
	
		<div id="splash"></div>
	
		<div id="menu">
			<ul>

				<?php foreach ($primary_links as $link): ?>
				<li><?php print $link; ?></li>
				<?php endforeach; ?>
			</ul>

		</div>
	

		<div id="primarycontent">
		
			<!-- primary content start -->
		
			<div class="post">
				<div class="header">
					<!-- Breadcrumb -->
					<?php print $breadcrumb; ?>
					<?php if ($title): ?>
						<h3><?php print $title; ?></h3>
					<?php endif; ?>
					<?php if ($date): ?>
						<div class="date"><?php print $date; ?></div>
					<?php endif; ?>
					
					<?php if ($tabs != ""): ?>
						<?php print $tabs; ?>
					<?php endif; ?>
					<?php if ($mission != ""): ?>
							<div class="site_mission featurebox"><?php print $mission ?></div>
					<?php endif; ?>
				</div>
				
				<?php if ($help != ""): ?>
					<!-- Help -->
					<p id="help"><?php print $help ?></p>
				<?php endif; ?>

				
				<?php if ($messages != ""): ?>
					<!-- Messages -->
					<div id="message"><?php print $messages ?></div>
				<?php endif; ?>
				<div class="content">
					<!-- first content start -->
						<?php print($content); ?>  
					<!-- first content end -->
				</div>			
			</div>
		</div>
		
		<div id="secondarycontent">
			<!-- secondary content start -->
				<!-- sidebar_right -->
				<?php print $sidebar_right; ?>&nbsp;
				<!-- sidebar_left -->
				<?php print $sidebar_left; ?>
			<!-- secondary content end -->
		</div>
	
		<div id="footer">
			<?php if ($footer_message) : ?>
				<?php print $footer_message;?>
			<?php endif; ?>
		</div>

	</div>

</div>

</body>
</html>
